#__author__ = 'pavelostrava'
import pandas as pd
import numpy as np
import random

import data_generation as dg
import time
import solverClass


def save_times(file_name, times_naive, times_top_k):
    file_path = '/Users/pavelrusnok/GoogleDrive/2015/top-k conference/' + file_name
    f = open(file_path, 'w')
    f.write("naive;topk\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write("\n")
    f.close()


def naive_approach(data, function, optimal_solution, ranking=1):
    data["weakRank"] = 0.0
    for i in range(len(data)):
        data["weakRank"][i] = function(list(data.ix[i][0:-1]), optimal_solution, aggregation_type=ranking)
    data = data.sort(["weakRank"], ascending=False)
    assert isinstance(data, pd.DataFrame)
    return data


def test1_data():
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-red.csv", ";")
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-white.csv", ";")
    wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/communities.data.txt", ",")

    minima = list()
    maxima = list()
    for column in wine.columns:
        minima.append(np.min(wine[column]))
        maxima.append(np.max(wine[column]))

    query_dimensions = list()
    query_optimals = list()
    for i in range(1000):
        query_dimensions.append(random.sample(range(6, 20), 2))
        a = random.uniform(minima[query_dimensions[i][0]], maxima[query_dimensions[i][0]])
        b = random.uniform(minima[query_dimensions[i][1]], maxima[query_dimensions[i][1]])
        #c = random.uniform(minima[query_dimensions[i][2]], maxima[query_dimensions[i][2]])
        query_optimals.append([a, b])
    times_top_k = list()
    times_naive = list()
    #for i in range(len(query_dimensions)):

    for i in range(1000):
        naive_data = pd.DataFrame(index=range(len(wine)), columns=range(2))
        naive_data[0] = wine[wine.columns[query_dimensions[i][0]]]
        naive_data[1] = wine[wine.columns[query_dimensions[i][1]]]
        #naive_data[2] = wine[wine.columns[query_dimensions[i][2]]]
        top_k_format = dg.topKFormat(naive_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(query_optimals[i], top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        times_top_k.append(time_top_k)
        start_time = time.time()
        naive_approach(naive_data, solverClass.weak_rank, query_optimals[i])
        time_naive = time.time() - start_time
        times_naive.append(time_naive)

    print(times_naive)
    print(times_top_k)
    f = open('/Users/pavelrusnok/GoogleDrive/2015/top-k conference/output_communities.csv', 'w')
    f.write("naive;topk\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write("\n")
    f.close()


def test2_ranges():
    columns = 3
    rows = 10000
    times_top_k = list()
    times_naive = list()
    for i in range(5, 1000  ):
        optimal_solution_value = i // 2
        optimal_solution = list([optimal_solution_value] * columns)
        values_range = i
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        naive_approach(random_data, solverClass.weak_rank, optimal_solution)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        print("finished test2 cycle ", str(i))
    save_times("output_ranges", times_naive, times_top_k)


def test3_data_size():
    columns = 3
    rows = 100
    values_range = 100
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rows < 10000000:
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        naive_approach(random_data, solverClass.weak_rank, optimal_solution)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rows *= 10
        print("finished test3 cycle", str(rows))
    save_times("output_data_size", times_naive, times_top_k)


def test3b_data_size():
    columns = 1
    rows = 100
    values_range = 100
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rows < 10000000:
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        naive_approach(random_data, solverClass.weak_rank, optimal_solution)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rows *= 10
        print("finished test3 cycle", str(rows))
    save_times("output_data_size1", times_naive, times_top_k)


def test4_aggregation():
    columns = 3
    rows = 1000
    values_range = 100
    optimal_solution = list([50] * columns)
    for i in range(1, 5):
        times_top_k = list()
        times_naive = list()
        for j in range(100):
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format, ranking=i)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            naive_approach(random_data, solverClass.weak_rank, optimal_solution, ranking=i)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
        save_times("output_aggregation_"+str(i), times_naive, times_top_k)
        print("finished test4 cycle i=", str(i))



if __name__ == '__main__':
    #test1_data()
    #test2_ranges()
    #test3_data_size()
    #test3b_data_size()
    test4_aggregation()
