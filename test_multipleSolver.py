import unittest
import data_generation as dg
import pandas as pd
# import numpy as np
from solverClass import BasicTopKSolver
from solverClass import MultipleSolver
from solverClass import weak_rank
from solverClass import get_tuple

__author__ = 'Pavel Rusnok'


class TestMultipleSolver(unittest.TestCase):
    def test_init(self):
        df = pd.DataFrame(index=range(3), columns=range(3))
        df[0] = pd.Series([1, 2, 3], index=df.index)
        df[1] = pd.Series([4, 2, 3], index=df.index)
        df[2] = pd.Series([1, 4, 3], index=df.index)
        df = dg.topKFormat(df)
        multiple = MultipleSolver([[3, 3, 3],[2, 2, 2]], df)
        self.assertEqual(2, len(multiple.list_of_solvers), "For two optimal solutions I have to create two solvers")
        s = multiple.list_of_solvers[0]
        self.assertIn([2, [3, 3, 3], 0], s.queue, "wrongly initialized queue")
        s = multiple.list_of_solvers[1]
        self.assertEqual(0, len(s.queue), "queue should be of zero length")
        self.assertEqual(0, len(s.seen_indices[0]), "wrongly updated seen_indices[0]")
        self.assertEqual(0, len(s.seen_indices[1]), "wrongly updated seen_indices[1]")
        self.assertEqual([2], s.seen_indices[2], "wrongly updated seen_indices[2]")
        self.assertEqual(0, len(s.partial_matches[0]), "wrongly update partial_matches[0]")
        self.assertEqual(1, len(s.partial_matches[1]), "wrongly update partial_matches[1]")
        self.assertEqual([1, 1, 0, 0, 1, 1], s.iterator, "wrong iterator")
        self.assertEqual([0, 2, 0, 2, 0, 2], s.margins, "wrong margins")
        self.assertEqual(0, s.direction, "wrong direction")

    def test_get_top_k(self):
        pass

    def test(self):
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
