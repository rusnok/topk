# __author__ = 'pavelostrava'
import pandas as pd
import numpy as np
import random

import data_generation as dg
import time
import solverClass
import solverClassFast

def save_times(file_name, times_naive, times_top_k):
    file_path = '/Users/pavelrusnok/GoogleDrive/2015/top-k conference/' + file_name
    f = open(file_path, 'w')
    f.write("naive;topk\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write("\n")
    f.close()

def save_times_rich(file_name, times_naive, times_top_k, times_top_k_rich):
    file_path = '/Users/pavelrusnok/GoogleDrive/2016/top-k results/' + file_name
    f = open(file_path, 'w')
    f.write("naive;topk;topkRich\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write(";")
        f.write(str(times_top_k_rich[i]))
        f.write("\n")
    f.close()

def save_times_journal(file_name, times_naive, times_top_k):
    file_path = '/Users/pavelrusnok/GoogleDrive/2016/top-k results/' + file_name
    f = open(file_path, 'w')
    f.write("naive;topk\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write("\n")
    f.close()

def naive_approach(data, function, optimal_solution, ranking=1):
    data["weakRank"] = 0.0
    for i in range(len(data)):
        data["weakRank"][i] = function(list(data.ix[i][0:-1]), optimal_solution, aggregation_type=ranking)
    data = data.sort(["weakRank"], ascending=False)
    assert isinstance(data, pd.DataFrame)
    return data


def naive_approach_multi(data, function, optimal_solution_multi, ranking=1):
    data["weakRank"] = 0.0
    for i in range(len(data)):
        data["weakRank"][i] = function(list(data.ix[i][0:-1]), optimal_solution_multi, aggregation_type=ranking)
    data = data.sort(["weakRank"], ascending=False)
    assert isinstance(data, pd.DataFrame)
    return data


def test1_data():
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-red.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-white.csv", ";")
    wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/communities.data.txt", ",")

    minima = list()
    maxima = list()
    for column in wine.columns:
        minima.append(np.min(wine[column]))
        maxima.append(np.max(wine[column]))

    query_dimensions = list()
    query_optimals = list()
    for i in range(1000):
        query_dimensions.append(random.sample(range(6, 20), 2))
        a = random.uniform(minima[query_dimensions[i][0]], maxima[query_dimensions[i][0]])
        b = random.uniform(minima[query_dimensions[i][1]], maxima[query_dimensions[i][1]])
        # c = random.uniform(minima[query_dimensions[i][2]], maxima[query_dimensions[i][2]])
        query_optimals.append([a, b])
    times_top_k = list()
    times_naive = list()
    # for i in range(len(query_dimensions)):

    for i in range(1000):
        naive_data = pd.DataFrame(index=range(len(wine)), columns=range(2))
        naive_data[0] = wine[wine.columns[query_dimensions[i][0]]]
        naive_data[1] = wine[wine.columns[query_dimensions[i][1]]]
        # naive_data[2] = wine[wine.columns[query_dimensions[i][2]]]
        top_k_format = dg.topKFormat(naive_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(query_optimals[i], top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        times_top_k.append(time_top_k)
        start_time = time.time()
        naive_approach(naive_data, solverClass.weak_rank, query_optimals[i])
        time_naive = time.time() - start_time
        times_naive.append(time_naive)

    print(times_naive)
    print(times_top_k)
    f = open('/Users/pavelrusnok/GoogleDrive/2015/top-k conference/output_communities.csv', 'w')
    f.write("naive;topk\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write("\n")
    f.close()


def test2_ranges():
    columns = 3
    rows = 10000
    times_top_k = list()
    times_naive = list()
    for i in range(5, 1000):
        optimal_solution_value = i // 2
        optimal_solution = list([optimal_solution_value] * columns)
        values_range = i
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        naive_approach(random_data, solverClass.weak_rank, optimal_solution)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        print("finished test2 cycle ", str(i))
    save_times("output_ranges", times_naive, times_top_k)


def test3_data_size():
    columns = 3
    rows = 100
    values_range = 100
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rows < 10000000:
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        naive_approach(random_data, solverClass.weak_rank, optimal_solution)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rows *= 10
        print("finished test3 cycle", str(rows))
    save_times("output_data_size", times_naive, times_top_k)


def test3b_data_size():
    columns = 2
    rows = 100
    values_range = 100
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rows < 100000:
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        naive_approach(random_data, solverClass.weak_rank, optimal_solution)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rows *= 10
        print("finished test3 cycle", str(rows))
    print(times_naive)
    print(times_top_k)
    save_times("output_data_size1", times_naive, times_top_k)


def test4_aggregation():
    columns = 3
    rows = 1000
    values_range = 100
    optimal_solution = list([50] * columns)
    for i in range(1, 5):
        times_top_k = list()
        times_naive = list()
        for j in range(100):
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format, ranking=i)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            naive_approach(random_data, solverClass.weak_rank, optimal_solution, ranking=i)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
        save_times("output_aggregation_" + str(i), times_naive, times_top_k)
        print("finished test4 cycle i=", str(i))


def test_debugging():
    columns = 3
    rows = 10000
    values_range = 100
    optimal_solution = list([50] * columns)
    optimal_solution2 = list([40] * columns)
    random_data = dg.randomDataCreationInteger(rows, columns, values_range)
    top_k_format = dg.topKFormat(random_data)
    s = solverClass.MultipleSolver([optimal_solution, optimal_solution2], top_k_format)
    top_k_result = s.get_top_k(10, top_k_format)
    for i in range(len(top_k_result)):
        print top_k_result[i]

    naive_result = naive_approach_multi(random_data, solverClass.weak_rank_multi, [optimal_solution, optimal_solution2])
    print naive_result[:10]

def test_multi_data_size(col = 1,rr=30,rowsInData = 10000000, startRows = 100):
    columns = col
    rows = startRows
    while rows < rowsInData:
        times_top_k = list()
        times_naive = list()
        for j in range(rr):
            values_range = rows
            optimal_solution = list(np.random.randint(5, rows-5, columns))
            optimal_solution2 = list(np.random.randint(5, rows-5, columns))
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            start_time = time.time()
            s = solverClass.MultipleSolver([optimal_solution, optimal_solution2], top_k_format)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            #naive_approach_multi(random_data, solverClass.weak_rank_multi,
             #                                   [optimal_solution, optimal_solution2])
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
            save_times_journal("output_data_size_multiple_col_" + str(columns) + "_rows_" + str(rows), times_naive,
                               times_top_k)
        print("finished multi_data cycle", str(rows))
        save_times_journal("output_data_size_multiple_col_"+str(columns) + "_rows_" + str(rows), times_naive, times_top_k)
        rows *= 10

def test1_multi_data():
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-red.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-white.csv", ";")
    wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/communities.data.txt", ",")

    minima = list()
    maxima = list()
    for column in wine.columns:
        minima.append(np.min(wine[column]))
        maxima.append(np.max(wine[column]))

    query_dimensions = list()
    query_optimals = list()
    query_optimals2 = list()
    for i in range(1000):
        query_dimensions.append(random.sample(range(6, 20), 2))
        a1 = random.uniform(minima[query_dimensions[i][0]], maxima[query_dimensions[i][0]])
        b1 = random.uniform(minima[query_dimensions[i][1]], maxima[query_dimensions[i][1]])
        # c = random.uniform(minima[query_dimensions[i][2]], maxima[query_dimensions[i][2]])
        query_optimals.append([a1, b1])
        a2 = random.uniform(minima[query_dimensions[i][0]], maxima[query_dimensions[i][0]])
        b2 = random.uniform(minima[query_dimensions[i][1]], maxima[query_dimensions[i][1]])
        # c = random.uniform(minima[query_dimensions[i][2]], maxima[query_dimensions[i][2]])
        query_optimals2.append([a2, b2])
    times_top_k = list()
    times_naive = list()
    # for i in range(len(query_dimensions)):

    for i in range(1000):
        naive_data = pd.DataFrame(index=range(len(wine)), columns=range(2))
        naive_data[0] = wine[wine.columns[query_dimensions[i][0]]]
        naive_data[1] = wine[wine.columns[query_dimensions[i][1]]]
        # naive_data[2] = wine[wine.columns[query_dimensions[i][2]]]
        top_k_format = dg.topKFormat(naive_data)
        start_time = time.time()
        s = solverClass.MultipleSolver([query_optimals[i], query_optimals2[i]], top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        times_top_k.append(time_top_k)
        start_time = time.time()
        naive_approach_multi(naive_data, solverClass.weak_rank_multi,
                             [query_optimals[i], query_optimals2[i]])
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        print(i)

    print(times_naive)
    print(times_top_k)
    f = open('/Users/pavelrusnok/GoogleDrive/2016/top-k results/output_multi_communities.csv', 'w')
    f.write("naive;topk\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write("\n")
    f.close()

def test_rich_format():
    columns = 5
    rows = 200
    times_top_k = list()
    times_naive = list()
    values_range = rows
    optimal_solution = list([rows * 0.5] * columns)
    optimal_solution2 = list([rows * 0.4] * columns)
    random_data = dg.randomDataCreationInteger(rows, columns, values_range)
    top_k_format = dg.topKFormat(random_data)
    top_k_format_rich = dg.topKFormatRich(random_data)
    s = solverClass.RichTopKSolver(optimal_solution,top_k_format_rich)
    t = solverClass.BasicTopKSolver(optimal_solution,top_k_format)
    t.get_top_k(10,top_k_format)

def rich_test3b_data_size():
    columns = 4
    rows = 100
    values_range = 100
    while rows < 1000000:
        times_top_k = list()
        times_naive = list()
        times_top_k_rich = list()
        for j in range(30):
            optimal_solution = list(np.random.randint(5,95,columns))
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            top_k_format_rich = dg.topKFormatRich(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            t = solverClass.RichTopKSolver(optimal_solution, top_k_format_rich)
            t.get_top_k(10, top_k_format_rich)
            time_top_k_rich = time.time() - start_time
            start_time = time.time()
            #naive_approach(random_data, solverClass.weak_rank, optimal_solution)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
            times_top_k_rich.append(time_top_k_rich)
        rows *= 10
        save_times_rich("output_rich_data_size_mean30_col4_"+str(rows), times_naive, times_top_k, times_top_k_rich)
        print("finished test3 cycle", str(rows))

def new_rich_test3b_data_size():
    columns = 5
    rows = 100
    values_range = 100
    while rows < 100000:
        times_top_k = list()
        times_naive = list()
        times_top_k_rich = list()
        for j in range(30):
            optimal_solution = list(np.random.randint(5,95,columns))
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            top_k_format_rich = dg.topKFormatRich(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            t = solverClass.RichTopKSolver(optimal_solution, top_k_format_rich)
            t.get_top_k(10, top_k_format_rich)
            time_top_k_rich = time.time() - start_time
            start_time = time.time()
            #naive_approach(random_data, solverClass.weak_rank, optimal_solution)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
            times_top_k_rich.append(time_top_k_rich)
        save_times_rich("output_rich_new_data_size_mean30_col_"+str(rows), times_naive, times_top_k, times_top_k_rich)
        print("finished test3 cycle", str(rows))
        rows *= 10

def new_init_test():
    columns = 2
    rows = 100
    values_range = 100
    while rows < 1000000:
        times_top_k = list()
        times_naive = list()
        times_top_k_rich = list()
        for j in range(1):
            optimal_solution = list(np.random.randint(40,60,columns))
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            top_k_format_rich = dg.topKFormatRich(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
            print(s.get_top_k(10, top_k_format))
            time_top_k = time.time() - start_time
            start_time = time.time()
            t = solverClass.BasicTopKSolverInit(optimal_solution, top_k_format)
            print(t.get_top_k(10, top_k_format))
            time_top_k_rich = time.time() - start_time
            start_time = time.time()
            naive_approach(random_data, solverClass.weak_rank, optimal_solution)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
            times_top_k_rich.append(time_top_k_rich)
        rows *= 10
        save_times_rich("output_rich_data_size_mean30_"+str(rows), times_naive, times_top_k, times_top_k_rich)
        print("finished test3 cycle", str(rows))

def rich_test_linear():
    columns = 2
    rows = 100
    values_range = 100
    optimal_solution = [150,4]

    while rows < 1000000:
        times_top_k = list()
        times_naive = list()
        times_top_k_rich = list()
        for j in range(30):
            random_data = dg.randomDataCreationLinear(rows)
            top_k_format = dg.topKFormat(random_data)
            top_k_format_rich = dg.topKFormatRich(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            t = solverClass.RichTopKSolver(optimal_solution, top_k_format_rich)
            t.get_top_k(10, top_k_format_rich)
            time_top_k_rich = time.time() - start_time
            start_time = time.time()
            naive_approach(random_data, solverClass.weak_rank, optimal_solution)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
            times_top_k_rich.append(time_top_k_rich)
        print("finished test3 cycle", str(rows))
        save_times_rich("output_rich_new_linear_"+str(rows), times_naive, times_top_k, times_top_k_rich)
        rows *= 10

def test_multi_top_x():
    columns = 2
    rows = 100000
    values_range = 100
    for k in [1,10,100,1000]:
        times_top_k = list()
        times_naive = list()
        for j in range(30):
            optimal_solution = list(np.random.randint(5,95,columns))
            optimal_solution2 = list(np.random.randint(5,95,columns))
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            start_time = time.time()
            s = solverClass.MultipleSolver([optimal_solution, optimal_solution2], top_k_format)
            s.get_top_k(k, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            naive_approach_multi(random_data, solverClass.weak_rank_multi,
                                                [optimal_solution, optimal_solution2])
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
        print("finished multi_data cycle", str(k))
        save_times_journal("output__multi_topk_" + str(k), times_naive, times_top_k)

def test_multi_aggregation():
    columns = 3
    rows = 1000
    values_range = 1000
    optimal_solution = list([30] * columns)
    optimal_solution2 = list([400] * columns)
    #optimal_solution3 = list([700] * columns)
    for i in range(1, 5):
        print(i)
        times_top_k = list()
        times_naive = list()
        for j in range(100):
            random_data = dg.randomDataCreationInteger(rows, columns, values_range)
            top_k_format = dg.topKFormat(random_data)
            start_time = time.time()
            s = solverClass.MultipleSolver([optimal_solution, optimal_solution2], top_k_format, ranking=i)
            s.get_top_k(10, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            s = solverClassFast.MultiTopKSolverFast([optimal_solution, optimal_solution2], top_k_format, ranking=i)
            s.get_top_k(10, top_k_format)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
        save_times_journal("output__new_multi_aggregation_col1_" + str(i), times_naive, times_top_k)
        print("finished test4 cycle i=", str(i))

def rich_test_multiNorm_data_size():
    columns = 3
    rows = 10000
    values_range = 100
    while rows < 100000:
        times_top_k = list()
        times_naive = list()
        times_top_k_rich = list()
        for j in range(5):
            #optimal_solution = list(np.random.randint(5,95,columns))
            optimal_solution = [50,50,-10]
            random_data = dg.randomDataCreationMultiNormal(rows,columns,[50,50,50],[400,400,400])
            top_k_format = dg.topKFormat(random_data)
            top_k_format_rich = dg.topKFormatRich(random_data)
            start_time = time.time()
            s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
            s.get_top_k(1, top_k_format)
            time_top_k = time.time() - start_time
            start_time = time.time()
            t = solverClass.RichTopKSolver(optimal_solution, top_k_format_rich)
            t.get_top_k(1, top_k_format_rich)
            time_top_k_rich = time.time() - start_time
            start_time = time.time()
            naive_approach(random_data, solverClass.weak_rank, optimal_solution)
            time_naive = time.time() - start_time
            times_naive.append(time_naive)
            times_top_k.append(time_top_k)
            times_top_k_rich.append(time_top_k_rich)
        rows *= 10
        save_times_rich("output_rich_new_data_multiNorm_"+str(rows), times_naive, times_top_k, times_top_k_rich)
        print("finished test3 cycle", str(rows))

def test_threshold_gradient():
    columns = 3
    rows = 100
    values_range = 100
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rows < 100000:
        values_range = rows
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        s = solverClass.ThresholdTopKSolver(optimal_solution, top_k_format,threshold_stack=10)
        s.get_top_k(10, top_k_format)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        print("finished test threshold cycle", str(rows))
        rows *= 10
    save_times_journal("threshold_gradient_10", times_naive, times_top_k)

def test_threshold_gradient_direction():
    columns = 3
    rows = 1000
    values_range = 100
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rows < 100000:
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        s = solverClass.BasicTopKSolver(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        s = solverClassFast.BasicTopKSolverFast(optimal_solution, top_k_format)
        s.get_top_k(10, top_k_format)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rows *= 10
        print("finished test3 cycle", str(rows))
    print(times_naive)
    print(times_top_k)

def multiple_tuning():
    columns = 3
    rows = 1000
    values_range = 100
    optimal_solution = list([50] * columns)
    optimal_solution1 = list([30] * columns)
    optimal_solution2 = list([60] * columns)
    random_data = dg.randomDataCreationInteger(rows, columns, values_range)
    top_k_format = dg.topKFormat(random_data)
    s = solverClassFast.MultiTopKSolverFast([optimal_solution1, optimal_solution2], top_k_format)
    p = solverClass.MultipleSolver([optimal_solution1, optimal_solution2], top_k_format)
    print(s.get_top_k(10,top_k_format))
    print(p.get_top_k(10, top_k_format))

def test_review_data():
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-red.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-white.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/communities.data.txt", ",")
    # wine = pd.read_table("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/auto-mpg.data.txt",header=None,delim_whitespace=True,na_values="?")
    # wine = pd.read_table("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/census-income.data.txt",header=None,delim_whitespace=True,na_values="?")
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/census-income.data.txt", ",", header=None)
    #wine = wine.iloc[:,[0,5,39]]
    wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/adult.data.txt", ",", header=None)
    wine = wine.iloc[:, [0, 10, 12]]

    minima = list()
    maxima = list()
    for column in wine.columns:
        minima.append(np.min(wine[column]))
        maxima.append(np.max(wine[column]))


    query_optimals = list()
    for i in range(30):
        a = random.uniform(minima[0]*1.1, maxima[0]*0.9)
        b = random.uniform(minima[1]*1.1, maxima[1]*0.9)
        c = random.uniform(minima[2]*1.1, maxima[2]*0.9)
        query_optimals.append([a, b, c])

    times_top_k = list()
    times_rich_topk = list()
    times_naive = list()
    # for i in range(len(query_dimensions)):
    naive_data = pd.DataFrame(index=range(len(wine)), columns=range(3))
    naive_data[0] = wine[wine.columns[0]]
    naive_data[1] = wine[wine.columns[1]]
    naive_data[2] = wine[wine.columns[2]]

    top_k_format = dg.topKFormat(naive_data)
    rich_topk_format = dg.topKFormatRich(naive_data)

    for i in range(30):
        start_time = time.time()
        s = solverClass.BasicTopKSolver(query_optimals[i], top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        times_top_k.append(time_top_k)
        start_time = time.time()
        r = solverClass.RichTopKSolver(query_optimals[i], rich_topk_format)
        r.get_top_k(10, rich_topk_format)
        time_top_k_rich = time.time() - start_time
        times_rich_topk.append(time_top_k_rich)
        start_time = time.time()
        naive_approach(naive_data, solverClass.weak_rank, query_optimals[i])
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        print(i)

    f = open('/Users/pavelrusnok/GoogleDrive/2015/top-k conference/output_review_census_3d.csv', 'w')
    f.write("naive;topk;topkRich\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write(";")
        f.write(str(times_rich_topk[i]))
        f.write("\n")
    f.close()

def test_review_data2():
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-red.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-white.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/communities.data.txt", ",")
    # wine = pd.read_table("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/auto-mpg.data.txt",header=None,delim_whitespace=True,na_values="?")
    # wine = pd.read_table("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/census-income.data.txt",header=None,delim_whitespace=True,na_values="?")
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/census-income.data.txt", ",", header=None)
    #wine = wine.iloc[:,[0,5,39]]
    wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/covtype.data.txt", ",", header=None)

    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/adult.data.txt", ",", header=None)
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/diabetic_data.csv", ",")

    wine = wine.iloc[:, [0, 9]]
    # wine = wine[["time_in_hospital","num_medications"]]
    means = list()
    minima = list()
    maxima = list()
    for column in wine.columns:
        means.append(np.median(wine[column]))
        minima.append(np.min(wine[column]))
        maxima.append(np.max(wine[column]))


    query_optimals = list()
    for i in range(10):
        a = random.uniform(means[0]-(means[0]-minima[0])*0.05, means[0]+(maxima[0]-means[0])*0.05)
        b = random.uniform(means[1]-(means[1]-minima[1])*0.05, means[1]+(maxima[1]-means[1])*0.05)
        #c = random.uniform(minima[2], maxima[2])
        print([a,b])
        query_optimals.append([a, b])
    times_top_k = list()
    times_rich_topk = list()
    times_naive = list()
    # for i in range(len(query_dimensions)):
    naive_data = pd.DataFrame(index=range(len(wine)), columns=range(2))
    naive_data[0] = wine[wine.columns[0]]
    naive_data[1] = wine[wine.columns[1]]
    #naive_data[2] = wine[wine.columns[2]]

    top_k_format = dg.topKFormat(naive_data)
    rich_topk_format = dg.topKFormatRich(naive_data)

    for i in range(10):
        start_time = time.time()
        s = solverClass.BasicTopKSolver(query_optimals[i], top_k_format)
        s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        times_top_k.append(time_top_k)
        start_time = time.time()
        r = solverClass.RichTopKSolver(query_optimals[i], rich_topk_format)
        r.get_top_k(10, rich_topk_format)
        time_top_k_rich = time.time() - start_time
        times_rich_topk.append(time_top_k_rich)
        start_time = time.time()
        naive_approach(naive_data, solverClass.weak_rank, query_optimals[i])
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        print(i)
        print(time_naive)
        print(time_top_k)
        print(time_top_k_rich)

    f = open('/Users/pavelrusnok/GoogleDrive/2015/top-k conference/output_review_wine.csv', 'w')
    f.write("naive;topk;topkRich\n")
    for i in range(len(times_naive)):
        f.write(str(times_naive[i]))
        f.write(";")
        f.write(str(times_top_k[i]))
        f.write(";")
        f.write(str(times_rich_topk[i]))
        f.write("\n")
    f.close()

def test_review_retrieved_tuples():
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-red.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/winequality-white.csv", ";")
    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/communities.data.txt", ",")
    # wine = pd.read_table("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/auto-mpg.data.txt",header=None,delim_whitespace=True,na_values="?")
    # wine = pd.read_table("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/census-income.data.txt",header=None,delim_whitespace=True,na_values="?")
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/census-income.data.txt", ",", header=None)
    #wine = wine.iloc[:,[0,5,39]]
    wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/covtype.data.txt", ",", header=None)

    # wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/adult.data.txt", ",", header=None)
    #wine = pd.read_csv("/Users/pavelrusnok/GoogleDrive/2015/top-k conference/diabetic_data.csv", ",")

    wine = wine.iloc[:, [0, 1, 2, 9]]
    # wine = wine[["time_in_hospital","num_medications"]]
    means = list()
    minima = list()
    maxima = list()
    for column in wine.columns:
        means.append(np.median(wine[column]))
        minima.append(np.min(wine[column]))
        maxima.append(np.max(wine[column]))


    query_optimals = list()
    for i in range(10):
        a = random.uniform(means[0]-(means[0]-minima[0])*0.05, means[0]+(maxima[0]-means[0])*0.05)
        b = random.uniform(means[1]-(means[1]-minima[1])*0.05, means[1]+(maxima[1]-means[1])*0.05)
        c = random.uniform(means[2] - (means[2] - minima[2]) * 0.05, means[2] + (maxima[2] - means[2]) * 0.05)
        d = random.uniform(means[3] - (means[3] - minima[3]) * 0.05, means[3] + (maxima[3] - means[3]) * 0.05)
        query_optimals.append([a, b, c, d])
    retrieved_N = list()

    naive_data = pd.DataFrame(index=range(len(wine)), columns=range(4))
    naive_data[0] = wine[wine.columns[0]]
    naive_data[1] = wine[wine.columns[1]]
    naive_data[2] = wine[wine.columns[2]]
    naive_data[3] = wine[wine.columns[3]]
    top_k_format = dg.topKFormat(naive_data)

    for i in range(10):
        start_time = time.time()
        s = solverClass.BasicTopKSolver(query_optimals[i], top_k_format)
        n = s.get_top_k(100, top_k_format)
        retrieved_N.append(n)
        print(i)
        print(n)
        print(time.time() - start_time)
    f = open('/Users/pavelrusnok/GoogleDrive/2015/top-k conference/output_review_retrieved_tuples_100_4D.csv', 'w')
    f.write("tuplesN\n")
    for i in range(len(retrieved_N)):
        f.write(str(retrieved_N[i]))
        f.write("\n")
    f.close()

def test_with_weights():
    columns = 2
    rows = 10000
    rweight = 1
    values_range = 1000
    optimal_solution = list([500] * columns)
    times_top_k = list()
    times_naive = list()
    while rweight < 101:
        random_data = dg.randomDataCreationInteger(rows, columns, values_range)
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        #s = solverClass.BasicTopKSolver(optimal_solution, top_k_format, weights = [1,1])
        #s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        r = solverClass.BasicTopKSolver(optimal_solution, top_k_format, weights=[1, rweight])
        r.get_top_k(10, top_k_format)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rweight += 1
        print("finished test with weight", str(rweight))
    save_times("output_weights_test_1000_values.csv", times_naive, times_top_k)

def test_with_weights_normal():
    columns = 2
    rows = 10000
    rweight = 1
    #values_range = 1000
    optimal_solution = list([50] * columns)
    times_top_k = list()
    times_naive = list()
    while rweight < 101:
        random_data = dg.randomDataCreationMultiNormal(rows,columns,[50,50],[400,400])
        top_k_format = dg.topKFormat(random_data)
        start_time = time.time()
        #s = solverClass.BasicTopKSolver(optimal_solution, top_k_format, weights = [1,1])
        #s.get_top_k(10, top_k_format)
        time_top_k = time.time() - start_time
        start_time = time.time()
        r = solverClass.BasicTopKSolver(optimal_solution, top_k_format, weights=[1, rweight])
        r.get_top_k(10, top_k_format)
        time_naive = time.time() - start_time
        times_naive.append(time_naive)
        times_top_k.append(time_top_k)
        rweight += 1
        print("finished test with weight", str(rweight))
    save_times("output_weights_test_normal_50_400_values.csv", times_naive, times_top_k)



if __name__ == '__main__':
    # multiple_tuning()
    # test1_data()
    # test2_ranges()
    # test3_data_size()
    # test3b_data_size()
    # test1_multi_data()
    # test_multi_data_size()
    # rich_test_linear()
    # test_multi_aggregation()
    # test_multi_data_size(3,30,startRows=1000000)
    # rich_test3b_data_size()
    # test_review_data2()
    # test_with_weights_normal()
    # new_rich_test3b_data_size()
    test_review_retrieved_tuples()
    # rich_test_multiNorm_data_size()
    #test_threshold_gradient_direction()
    # udelame flat data nebo linear data a budeme nahodne vybirat
    # query z [0,1] nasobku values_range vice krat a budeme se koukat
    # jak stabilini je cas se kterym se vraci odpoved
    # Jeste muzu kombinovat ruzne agregacni funkce a jejich blbuvzdornost
        # na vahy cili pro jednu agregacni funkci jedno query a spoustu vah
        # a zase prumer casovy a rozptyl.
