Experimental implementation of a top-k algorithm for non-monotonic ranking functions,
described in the article:
"A Top-K Retrieval algorithm based on a decomposition of ranking functions"

https://www.sciencedirect.com/science/article/pii/S002002551830714X

The algorithm is implemented in solverClass.py and the results presented
in the article might be obtained by running particular tests implemented
in journal_article_tests.py, but some absolute directory pathways have to be changed.
