import pandas as pd
import numpy as np
from operator import itemgetter


def weak_rank(values, optimal_values, weights=1, aggregation_type=1):
    """
    weakRank calculates the value of Weak Ranking function
    and returns the result.
    :param values: Values given to the function for calculating the weak rank
    :param optimal_values: Parameters of the function (where the maximum is)
    :param weights: weights are giving various dimensions differing importance
    :param aggregation_type: type of aggregation: 1=sum, 2=min, 3=max, 4=product
    """
    if weights == 1:
        weights = list([1] * len(optimal_values))
    assert len(optimal_values) == len(values), "Optimal values are of wrong length: %r"
    return_value = 0

    if aggregation_type == 1:
        return_value = 0
        for i in range(len(values)):
            return_value -= weights[i] * abs(values[i] - optimal_values[i])
    elif aggregation_type == 2:
        return_value = 0
        for i in range(len(values)):
            distance_in_one_dimension = -weights[i] * abs(values[i] - optimal_values[i])
            if return_value > distance_in_one_dimension:
                return_value = distance_in_one_dimension
    elif aggregation_type == 3:
        return_value = -np.inf
        for i in range(len(values)):
            distance_in_one_dimension = -weights[i] * abs(values[i] - optimal_values[i])
            if return_value < distance_in_one_dimension:
                return_value = distance_in_one_dimension
    elif aggregation_type == 4:
        return_value = -1
        for i in range(len(values)):
            return_value *= weights[i] * abs(values[i] - optimal_values[i])

    return return_value

def weak_rank_multi(values, optimal_values_multi, weights = 1, aggregation_type=1):
    """
    weak_rank_multi calculates the value of combined ranking functions. This function
    is only for naive approach because in top-k approach we devide the search between
    many Basic(Partial) top-k solvers and they use just weak ranking functions.
    :param values: values from data frame
    :param optimal_values_multi:
    :param weights:
    :param aggregation_type:
    :return:
    """
    if weights == 1:
        weights = list([1] * len(optimal_values_multi[0]))
    assert len(optimal_values_multi[0]) == len(values), "Optimal values are of wrong length: %r"
    return_value = 0

    if aggregation_type == 1:
        return_value_partial = list([0] * len(optimal_values_multi))
        for j in range(len(optimal_values_multi)):
            for i in range(len(values)):
                return_value_partial[j] -= weights[i] * abs(values[i] - optimal_values_multi[j][i])
        return_value = max(return_value_partial)
    elif aggregation_type == 2:
        return_value_partial = list([0] * len(optimal_values_multi))
        for j in range(len(optimal_values_multi)):
            for i in range(len(values)):
                distance_in_one_dimension = -weights[i] * abs(values[i] - optimal_values_multi[j][i])
                if return_value_partial[j] > distance_in_one_dimension:
                    return_value_partial[j] = distance_in_one_dimension
        return_value = max(return_value_partial)
    elif aggregation_type == 3:
        return_value_partial = list([-np.inf] * len(optimal_values_multi))
        for j in range(len(optimal_values_multi)):
            for i in range(len(values)):
                distance_in_one_dimension = -weights[i] * abs(values[i] - optimal_values_multi[j][i])
                if return_value_partial[j] < distance_in_one_dimension:
                    return_value_partial[j] = distance_in_one_dimension
        return_value = max(return_value_partial)
    elif aggregation_type == 4:
        return_value_partial = list([-1] * len(optimal_values_multi))
        for j in range(len(optimal_values_multi)):
            for i in range(len(values)):
                return_value_partial[j] *= weights[i] * abs(values[i] - optimal_values_multi[j][i])
        return_value = max(return_value_partial)
    return return_value


def get_tuple(index, data):
    """
    :rtype : list
    :param index: index of a tuple in data
    :param data: list of pandas DataFrames
    """
    line_in_data = []
    for i in range(len(data)):
        line_in_data.append(data[i][i][index])
    return line_in_data


class BasicTopKSolverFast:
    def __init__(self, optimal_solution, data, ranking=1, threshold_gradient=0):
        """This initialization function needs optimal solution of topK problem
        and data (list of pandas data frames). The position of optimal solution
        is searched and its actual or possible place is given.
        :type self: BasicTopKSolver
        :param optimal_solution: is list of values (integer or real)
        :param data: is a list of pandas data frames (topK format)"""

        assert isinstance(data, list)
        assert isinstance(data[0], pd.DataFrame)
        # dimension of top-k problem
        self.columns = len(data)
        self.rows = len(data[0])
        self.directions = 2 * self.columns

        # seen_indices is a list of lists of indices in every dimension
        # where I have already looked during the search. Indices are not
        # linearly ordered in top-k format contrary to lines.
        self.seen_indices = []

        # partial_matches is a list of 2-tuple,...,n-1-tuples, where n is
        # the dimension of the data. partial_matches store "partial"
        # tuples, for which joint condition holds
        self.partial_matches = []

        # Threshold stores the current cut needed during top-k search.
        self.threshold = 0
        self.threshold_gradient = threshold_gradient

        # iterator is a list of lines which show in which direction
        # I may continue my search. Iterators (lines) are integers linearly ordered but indices are not
        self.iterator = []

        # tuples that fulfill the joint condition are stored in Q and
        # waiting till the threshold allows them to be returned
        self.queue = []

        # number of tuples added to queue (not necessarily present in queue)
        self.potential = 0

        # margins is a list of 2-tuples that are constant and are equal
        # to 0 and number of rows in data
        self.margins = []

        # direction is an index that shows in which direction we should look
        # further it may have 2*n values, where n is the dimension of data
        self.direction = 0
        self.current_direction = 0
        self.current_dimension = 0

        # We also need the optimal solution to be still accessible for weak
        # ranking function
        self.optimal_solution = optimal_solution

        # Type of weak ranking function to be used.
        self.ranking_type = ranking

        for i in range(self.columns):
            self.margins.append(0)
            self.margins.append(self.rows - 1)
            self.partial_matches.append([])
            for j in range(self.rows):
                if data[i][i][data[i].index[j]] >= optimal_solution[i]:
                    self.iterator.append(j)
                    self.iterator.append(j)
                    self.seen_indices.append(list([data[i].index[j]]))
                    break
            if (i * 2) == len(self.iterator):
                self.iterator.append(self.rows - 1)
                self.iterator.append(self.rows - 1)
                self.seen_indices.append(list([data[i].index[self.rows - 1]]))

        # Now we have to go through seen_indices and
        # initialize partial_matches
        self.partial_matches[0].append(self.seen_indices[0].pop())
        for i in range(self.columns - 1):
            if len(self.partial_matches[i]) > 0 and self.partial_matches[i][0] == self.seen_indices[i + 1][0]:
                self.partial_matches[i + 1].append(self.partial_matches[i].pop())
                self.seen_indices[i + 1].pop()

        # If optimal solution is present in the data we immediately put it into queue
        if 0 < len(self.partial_matches[self.columns - 1]):
            self.add_tuple_to_queue(self.partial_matches[self.columns - 1].pop(), data)

    def add_tuple_to_queue(self, index, data):
        """ This function adds a tuple to Q which fulfills the joint condition together with
        the value of weak
        :type data: list
        :param index: index of line in data
        :param data: list of pandas DataFrames
        """
        data_tuple = get_tuple(index, data)
        tuple_to_queue = [index, data_tuple,
                          weak_rank(data_tuple, self.optimal_solution, aggregation_type=self.ranking_type)]
        if len(self.queue) == 0:
            self.queue.append(tuple_to_queue)
        elif len(self.queue) == 1:
            if self.queue[0][2] > tuple_to_queue[2]:
                self.queue.insert(1, tuple_to_queue)
            else:
                self.queue.insert(0, tuple_to_queue)
        else:
            left = 0
            right = len(self.queue) - 1
            while left <= right:
                middle = (right + left) / 2
                if self.queue[middle][2] == tuple_to_queue[2]:
                    left = middle
                    break
                elif self.queue[middle][2] < tuple_to_queue[2]:
                    right = middle - 1
                else:
                    left = middle + 1
                    # for i in range(len(self.queue)):
                    #    if tuple_to_queue[2] > self.queue[i][2]:
                    #        index_for_inserting = i
                    #        break
            self.queue.insert(left, tuple_to_queue)

    def get_next_index(self, data):
        """
         This functions tests whether we are not at the border of one of the dimensions.
         It is useless when we start in center, but a must when we start near some border.
        """
        next_index = -1
        if self.potential>5:
            if 0 < abs(self.margins[self.threshold_gradient]-self.iterator[self.threshold_gradient]):
                self.iterator[self.threshold_gradient] += np.sign(self.margins[self.threshold_gradient] - self.iterator[self.threshold_gradient])
                self.current_direction = self.threshold_gradient
                self.current_dimension = self.current_direction // 2
                next_index = data[self.current_dimension].index[self.iterator[self.current_direction]]
                return next_index

        for j in range(self.direction, self.direction + self.directions):
            i = j % self.directions
            if 0 < abs(self.margins[i] - self.iterator[i]):
                self.iterator[i] += np.sign(self.margins[i] - self.iterator[i])
                self.current_direction = i
                self.current_dimension = self.current_direction // 2
                self.direction = (self.current_direction + 1) % self.directions
                break
        # Now i get the index from data in current dimension
        next_index = data[self.current_dimension].index[self.iterator[self.current_direction]]
        return next_index

    def update_partial_matches(self, new_index, dimension):
        """
        This is a help function for add_next_index, which adds new index into seen_indices
        or partial_matches based on previous searches (current state) of seen_indices and partial_matches
        """
        next_partial_match = False
        if dimension < (self.columns - 1):
            for i in range(len(self.seen_indices[dimension + 1])):
                if new_index == self.seen_indices[dimension + 1][i]:
                    next_partial_match = True
                    self.seen_indices[dimension + 1].pop(i)
                    break

        if next_partial_match:
            self.update_partial_matches(new_index, dimension + 1)
        else:
            self.partial_matches[dimension].append(new_index)

    def add_next_index(self, next_index):
        """
        This function takes next_index, looks at the previous dimension to partial matches
        and then if it matches with previous matches it tries to match with next dimension
        seen_indices as far as possible with update_partial_matches. The method stores next_index
        into partial_matches or seen_indices if it manages to match or not.
        """
        new_partial_match = False
        if self.current_dimension == 0:
            new_partial_match = True
        else:
            for index in range(len(self.partial_matches[self.current_dimension - 1])):
                if self.partial_matches[self.current_dimension - 1][index] == next_index:
                    self.partial_matches[self.current_dimension - 1].pop(index)
                    new_partial_match = True
                    break

        if new_partial_match:
            self.update_partial_matches(next_index, self.current_dimension)
        else:
            self.seen_indices[self.current_dimension].append(next_index)

    def update_threshold(self, data):
        """
        This function updates threshold
        """
        projections = [-np.inf]
        for i in range(len(self.iterator)):
            if 0 < abs(self.margins[i] - self.iterator[i]):
                dimension = i // 2
                row = self.iterator[i]
                projection = list(self.optimal_solution)
                projection[dimension] = data[dimension][dimension][data[dimension].index[row]]
                projections.append(weak_rank(projection, self.optimal_solution, aggregation_type=self.ranking_type))
        self.threshold = max(projections)
        if self.threshold > -np.inf:
            self.threshold_gradient = projections.index(self.threshold)-1



    def get_next_top_k_element(self, data):
        """
         This function gets next tuple from top k tuples in data based on optimal value.
        """
        # If there is already something in queue we can pop it if it is above
        # current threshold
        if len(self.queue) > 0:
            if self.queue[0][2] >= self.threshold:
                return self.queue.pop(0)

        while True:
            while 0 == len(self.partial_matches[self.columns - 1]):
                next_index = self.get_next_index(data)
                if next_index == -1:
                    break
                else:
                    self.add_next_index(next_index)
            next_tuple_logic = (0 < len(self.partial_matches[self.columns - 1]))

            if next_tuple_logic:
                self.update_threshold(data)
                next_tuple_index = self.partial_matches[self.columns - 1].pop()
                self.add_tuple_to_queue(next_tuple_index, data)
                self.potential += 1

            if len(self.queue) > 0:
                if self.queue[0][2] >= self.threshold:
                    return self.queue.pop(0)
                else:
                    if not next_tuple_logic:
                        return self.queue.pop(0)
            else:
                if not next_tuple_logic:
                    return None

    def get_top_k(self, k, data):
        top_k_list = []
        while True:
            next_element = self.get_next_top_k_element(data)
            if next_element is None:
                break
            top_k_list.append(next_element)
            if k == len(top_k_list):
                break
        return top_k_list

class MultiTopKSolverFast:
    def __init__(self, optimal_solution, data, ranking=1, threshold_gradient=0):
        """This initialization function needs optimal solution of topK problem
        and data (list of pandas data frames). The position of optimal solution
        is searched and its actual or possible place is given.
        :type self: BasicTopKSolver
        :param optimal_solution: is list of values (integer or real)
        :param data: is a list of pandas data frames (topK format)"""

        assert isinstance(data, list)
        assert isinstance(data[0], pd.DataFrame)
        # dimension of top-k problem
        self.columns = len(data)
        self.rows = len(data[0])
        self.directions = 2 * self.columns * len(optimal_solution)
        self.directions_modulo = 2 * self.columns

        # seen_indices is a list of lists of indices in every dimension
        # where I have already looked during the search. Indices are not
        # linearly ordered in top-k format contrary to lines.
        self.seen_indices = []

        # partial_matches is a list of 2-tuple,...,n-1-tuples, where n is
        # the dimension of the data. partial_matches store "partial"
        # tuples, for which joint condition holds
        self.partial_matches = []

        # Threshold stores the current cut needed during top-k search.
        self.threshold = 0
        self.threshold_gradient = threshold_gradient

        # iterator is a list of lines which show in which direction
        # I may continue my search. Iterators (lines) are integers linearly ordered but indices are not
        self.iterator = []

        # tuples that fulfill the joint condition are stored in Q and
        # waiting till the threshold allows them to be returned
        self.queue = []

        # number of tuples added to queue (not necessarily present in queue)
        self.potential = 0

        # margins is a list of 2-tuples that are constant and are equal
        # to 0 and number of rows in data
        self.margins = []

        # direction is an index that shows in which direction we should look
        # further it may have 2*n values, where n is the dimension of data
        self.direction = 0
        self.current_direction = 0
        self.current_dimension = 0

        # We also need the optimal solution to be still accessible for weak
        # ranking function
        self.optimal_solution = optimal_solution

        # Type of weak ranking function to be used.
        self.ranking_type = ranking

        for i in range(self.columns):
            self.partial_matches.append([])
            self.seen_indices.append([])

        for os in optimal_solution:
            for i in range(self.columns):
                self.margins.append(0)
                self.margins.append(self.rows - 1)
                for j in range(self.rows):
                    if data[i][i][data[i].index[j]] >= os[i]:
                        self.iterator.append(j)
                        self.iterator.append(j)
                        self.seen_indices[i].append(data[i].index[j])
                        break
                if (i * 2) == len(self.iterator):
                    self.iterator.append(self.rows - 1)
                    self.iterator.append(self.rows - 1)
                    self.seen_indices[i].append(data[i].index[self.rows - 1])
            self.partial_matches[0].append(self.seen_indices[0].pop())
        # Now we have to go through seen_indices and
        # initialize partial_matches


        for i in range(self.columns - 1):
            if len(self.partial_matches[i]) > 0 and self.partial_matches[i][0] == self.seen_indices[i + 1][0]:
                self.partial_matches[i + 1].append(self.partial_matches[i].pop())
                self.seen_indices[i + 1].pop()

        # If optimal solution is present in the data we immediately put it into queue
        if 0 < len(self.partial_matches[self.columns - 1]):
            self.add_tuple_to_queue(self.partial_matches[self.columns - 1].pop(), data)

    def add_tuple_to_queue(self, index, data):
        """ This function adds a tuple to Q which fulfills the joint condition together with
        the value of weak
        :type data: list
        :param index: index of line in data
        :param data: list of pandas DataFrames
        """
        data_tuple = get_tuple(index, data)
        tuple_to_queue = [index, data_tuple,
                          weak_rank_multi(data_tuple, self.optimal_solution, aggregation_type=self.ranking_type)]
        if len(self.queue) == 0:
            self.queue.append(tuple_to_queue)
        elif len(self.queue) == 1:
            if self.queue[0][2] > tuple_to_queue[2]:
                self.queue.insert(1, tuple_to_queue)
            else:
                self.queue.insert(0, tuple_to_queue)
        else:
            left = 0
            right = len(self.queue) - 1
            while left <= right:
                middle = (right + left) / 2
                if self.queue[middle][2] == tuple_to_queue[2]:
                    left = middle
                    break
                elif self.queue[middle][2] < tuple_to_queue[2]:
                    right = middle - 1
                else:
                    left = middle + 1
                    # for i in range(len(self.queue)):
                    #    if tuple_to_queue[2] > self.queue[i][2]:
                    #        index_for_inserting = i
                    #        break
            self.queue.insert(left, tuple_to_queue)

    def get_next_index(self, data):
        """
         This functions tests whether we are not at the border of one of the dimensions.
         It is useless when we start in center, but a must when we start near some border.
        """
        next_index = -1
        if self.potential>5:
            if 0 < abs(self.margins[self.threshold_gradient]-self.iterator[self.threshold_gradient]):
                self.iterator[self.threshold_gradient] += np.sign(self.margins[self.threshold_gradient] - self.iterator[self.threshold_gradient])
                self.current_direction = self.threshold_gradient
                self.current_dimension = (self.current_direction % self.directions_modulo ) // 2
                next_index = data[self.current_dimension].index[self.iterator[self.current_direction]]
                return next_index

        for j in range(self.direction, self.direction + self.directions):
            i = j % self.directions
            if 0 < abs(self.margins[i] - self.iterator[i]):
                self.iterator[i] += np.sign(self.margins[i] - self.iterator[i])
                self.current_direction = i
                self.current_dimension = (self.current_direction % self.directions_modulo ) // 2
                self.direction = (self.current_direction + 1) % self.directions
                break
        # Now i get the index from data in current dimension
        next_index = data[self.current_dimension].index[self.iterator[self.current_direction]]
        return next_index

    def update_partial_matches(self, new_index, dimension):
        """
        This is a help function for add_next_index, which adds new index into seen_indices
        or partial_matches based on previous searches (current state) of seen_indices and partial_matches
        """
        next_partial_match = False
        if dimension < (self.columns - 1):
            for i in range(len(self.seen_indices[dimension + 1])):
                if new_index == self.seen_indices[dimension + 1][i]:
                    next_partial_match = True
                    self.seen_indices[dimension + 1].pop(i)
                    break

        if next_partial_match:
            self.update_partial_matches(new_index, dimension + 1)
        else:
            self.partial_matches[dimension].append(new_index)

    def add_next_index(self, next_index):
        """
        This function takes next_index, looks at the previous dimension to partial matches
        and then if it matches with previous matches it tries to match with next dimension
        seen_indices as far as possible with update_partial_matches. The method stores next_index
        into partial_matches or seen_indices if it manages to match or not.
        """
        new_partial_match = False
        if self.current_dimension == 0:
            new_partial_match = True
        else:
            for index in range(len(self.partial_matches[self.current_dimension - 1])):
                if self.partial_matches[self.current_dimension - 1][index] == next_index:
                    self.partial_matches[self.current_dimension - 1].pop(index)
                    new_partial_match = True
                    break

        if new_partial_match:
            self.update_partial_matches(next_index, self.current_dimension)
        else:
            self.seen_indices[self.current_dimension].append(next_index)

    def update_threshold(self, data):
        """
        This function updates threshold
        """
        projections = [-np.inf]
        for i in range(len(self.iterator)):
            if 0 < abs(self.margins[i] - self.iterator[i]):
                dimension = (i % self.directions_modulo ) // 2
                row = self.iterator[i]
                projection = list(self.optimal_solution[i // self.directions_modulo])
                projection[dimension] = data[dimension][dimension][data[dimension].index[row]]
                projections.append(weak_rank_multi(projection, self.optimal_solution, aggregation_type=self.ranking_type))
        self.threshold = max(projections)
        if self.threshold > -np.inf:
            self.threshold_gradient = projections.index(self.threshold)-1


    def get_next_top_k_element(self, data):
        """
         This function gets next tuple from top k tuples in data based on optimal value.
        """
        # If there is already something in queue we can pop it if it is above
        # current threshold
        if len(self.queue) > 0:
            if self.queue[0][2] >= self.threshold:
                return self.queue.pop(0)

        while True:
            while 0 == len(self.partial_matches[self.columns - 1]):
                next_index = self.get_next_index(data)
                if next_index == -1:
                    break
                else:
                    self.add_next_index(next_index)
            next_tuple_logic = (0 < len(self.partial_matches[self.columns - 1]))

            if next_tuple_logic:
                self.update_threshold(data)
                next_tuple_index = self.partial_matches[self.columns - 1].pop()
                self.add_tuple_to_queue(next_tuple_index, data)
                self.potential += 1

            if len(self.queue) > 0:
                if self.queue[0][2] >= self.threshold:
                    return self.queue.pop(0)
                else:
                    if not next_tuple_logic:
                        return self.queue.pop(0)
            else:
                if not next_tuple_logic:
                    return None

    def get_top_k(self, k, data):
        top_k_list = []
        while True:
            next_element = self.get_next_top_k_element(data)
            if next_element is None:
                break
            top_k_list.append(next_element)
            if k == len(top_k_list):
                break
        return top_k_list
