import unittest
import data_generation as dg
import pandas as pd
# import numpy as np
from solverClass import RichTopKSolver
from solverClass import weak_rank
from solverClass import get_tuple

__author__ = 'Pavel Rusnok'



class TestRichTopKSolver(unittest.TestCase):
    def test_init(self):
        """
        This test tests the initialization method of BasicTopKSolver, which
        searches for the placement of optimal solution.
        """
        random_data = dg.randomDataCreationInteger(4, 4, 4)
        top_k_format = dg.topKFormatRich(random_data)
        s = RichTopKSolver([5, 5, 5, 5], top_k_format)
        self.assertEqual(list([3] * 8), s.iterator)
        r = RichTopKSolver(list([0] * 4), top_k_format)
        self.assertEqual(list([0] * 8), r.iterator)
        df = pd.DataFrame(index=range(3), columns=range(3))
        df[0] = pd.Series([1, 2, 3], index=df.index)
        df[1] = pd.Series([4, 2, 3], index=df.index)
        df[2] = pd.Series([1, 4, 3], index=df.index)
        df = dg.topKFormatRich(df)
        s = RichTopKSolver([3, 3, 3], df)
        self.assertIn([2, [3, 3, 3], 0], s.queue, "wrongly initialized queue")
        s = RichTopKSolver([2, 2, 2], df)
        self.assertEqual([1, 1, 0, 0, 1, 1], s.iterator, "wrong iterator")
        self.assertEqual([0, 2, 0, 2, 0, 2], s.margins, "wrong margins")
        self.assertEqual(0, s.direction, "wrong direction")

    def test_add_tuple_to_queue(self):
         df = pd.DataFrame(index=range(3), columns=range(3))
         df[0] = pd.Series([1, 2, 3], index=df.index)
         df[1] = pd.Series([4, 2, 3], index=df.index)
         df[2] = pd.Series([1, 4, 3], index=df.index)
         df = dg.topKFormatRich(df)
         s = RichTopKSolver([2, 2, 2], df)
         s.add_tuple_to_queue(1, df)
         self.assertIn([1, [2, 2, 4], -2], s.queue, "wrongly added to queue")
         s.add_tuple_to_queue(2, df)
         self.assertIn([2, [3, 3, 3], -3], s.queue, "wrongly added to queue")

    def test_get_next_index(self):
         df = pd.DataFrame(index=range(3), columns=range(3))
         df[0] = pd.Series([1, 2, 3], index=df.index)
         df[1] = pd.Series([4, 2, 3], index=df.index)
         df[2] = pd.Series([1, 4, 3], index=df.index)
         df = dg.topKFormatRich(df)
         s = RichTopKSolver([2, 2, 2], df)
         self.assertEqual(0, s.get_next_index(df), "wrongly chosen index")
         self.assertEqual(2, s.get_next_index(df), "wrongly chosen index")
         self.assertEqual(2, s.get_next_index(df), "wrongly chosen index")

    def test_update_threshold(self):
        df = pd.DataFrame(index=range(3), columns=range(3))
        df[0] = pd.Series([1, 2, 3], index=df.index)
        df[1] = pd.Series([4, 2, 3], index=df.index)
        df[2] = pd.Series([1, 4, 3], index=df.index)
        df = dg.topKFormatRich(df)
        s = RichTopKSolver([2, 2, 2], df)
        s.update_threshold(df)
        self.assertEqual(0, s.threshold, "wrongly updated threshold 1")
        s.get_next_index(df)
        s.get_next_index(df)
        s.get_next_index(df)
        s.update_threshold(df)
        self.assertEqual(-1, s.threshold, "wrongly updated threshold 2")
        s = RichTopKSolver([0, 0, 0], df)
        s.update_threshold(df)
        self.assertEqual(-1, s.threshold, "wrongly updated threshold 3")
        s.get_next_index(df)
        s.get_next_index(df)
        s.get_next_index(df)
        s.update_threshold(df)
        self.assertEqual(-2, s.threshold, "wrongly updated threshold 2")


    def test_get_next_top_k_element(self):
        df = pd.DataFrame(index=range(3), columns=range(3))
        df[0] = pd.Series([1, 2, 3], index=df.index)
        df[1] = pd.Series([4, 2, 3], index=df.index)
        df[2] = pd.Series([1, 4, 3], index=df.index)
        df = dg.topKFormatRich(df)
        s = RichTopKSolver([2, 2, 2], df)
        top_k_element = s.get_next_top_k_element(df)
        self.assertEqual([1, [2, 2, 4], -2], top_k_element, "Wrong first top-k element")
        top_k_element = s.get_next_top_k_element(df)
        self.assertEqual([2, [3, 3, 3], -3], top_k_element, "Wrong second top-k element")
        self.assertEqual([0, [1, 4, 1], -4], s.get_next_top_k_element(df), "Wrong third top-k element")

    def test_get_top_k(self):
        df = pd.DataFrame(index=range(3), columns=range(3))
        df[0] = pd.Series([1, 2, 3], index=df.index)
        df[1] = pd.Series([4, 2, 3], index=df.index)
        df[2] = pd.Series([1, 4, 3], index=df.index)
        df = dg.topKFormatRich(df)
        s = RichTopKSolver([2, 2, 2], df)
        top_k_result = [[1, [2, 2, 4], -2], [2, [3, 3, 3], -3], [0, [1, 4, 1], -4]]
        self.assertEqual(top_k_result, s.get_top_k(3, df), "Wrong top-k result")

    def test(self):
        self.assertTrue(True)

if __name__ == '__main__':
    unittest.main()
