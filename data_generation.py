__author__ = 'pavelostrava'
import pandas as pd
import numpy as np

def randomDataCreation(numberOfRows, numberOfColumns,rangeOfValues):
    """
    randomDataCreation creates pandas.Dataframe object with numberOfRows rows
    and numberOfColumns columns filled with float values from 0 to rangeOfValues.
    Values are generated with uniform distribution.
    """
    df = pd.DataFrame(index=range(numberOfRows), columns=range(numberOfColumns))
    for i in range(numberOfColumns):
        df[i] = pd.Series(np.random.uniform(1,rangeOfValues,numberOfRows), index=df.index)
    return df

def randomDataCreationNormal(numberOfRows, numberOfColumns,rangeOfValues):
    """
    randomDataCreation creates pandas.Dataframe object with numberOfRows rows
    and numberOfColumns columns filled with float values from 0 to rangeOfValues.
    Values are generated with normal distribution.
    """
    df = pd.DataFrame(index=range(numberOfRows), columns=range(numberOfColumns))
    for i in range(numberOfColumns):
        df[i] = pd.Series(np.random.normal(rangeOfValues/2,rangeOfValues/4,numberOfRows), index=df.index)
    return df

def randomDataCreationInteger(numberOfRows, numberOfColumns,rangeOfValues):
    """
    randomDataCreation creates pandas.Dataframe object with numberOfRows rows
    and numberOfColumns columns filled with integer values from 0 to rangeOfValues.
    Values are generated with uniform distribution.
    """
    df = pd.DataFrame(index=range(numberOfRows), columns=range(numberOfColumns))
    for i in range(numberOfColumns):
        df[i] = pd.Series(np.random.random_integers(1,rangeOfValues,numberOfRows), index=df.index)
    return df

def randomDataCreationMultiNormal(numberOfRows, numberOfColumns,means,vars):
    """
    randomDataCreation creates pandas.Dataframe object with numberOfRows rows
    and numberOfColumns columns filled with integer values from 0 to rangeOfValues.
    Values are generated with uniform distribution.
    """
    cov = []
    for i in range(len(means)):
        zeroLine = list([0]*numberOfColumns)
        zeroLine[i] = vars[i]
        cov.append(zeroLine)

    df = pd.DataFrame(np.random.multivariate_normal(means, cov, numberOfRows))
    return df

def randomDataCreationNormalNodes(numberOfRows, numberOfColumns,rangeOfValues):
    """
    randomDataCreation creates pandas.Dataframe object with numberOfRows rows
    and numberOfColumns columns filled with real values from 0 to rangeOfValues.
    Values are generated with normal distributions with various means.
    """
    cov = []
    for i in range(len(means)):
        zeroLine = list([0]*numberOfColumns)
        zeroLine[i] = vars[i]
        cov.append(zeroLine)

    df = pd.DataFrame(np.random.multivariate_normal(means, cov, numberOfRows))
    return df

def randomDataCreationNicolas(numberOfRows, numberOfColumns, rangeOfValues):
    """
    randomDataCreation creates pandas.Dataframe object with numberOfRows rows
    and numberOfColumns columns filled with integer values from 0 to rangeOfValues.
    Values are generated with uniform distribution.
    """
    df = pd.DataFrame(index=range(numberOfRows), columns=range(numberOfColumns))
    for i in range(numberOfColumns):
        df[i] = pd.Series(np.random.random_integers(1,rangeOfValues[i],numberOfRows), index=df.index)
    return df

def randomDataCreationLinear(numberOfRows):
    """
    We are creating random data describing set of flats. First we generate the number of rooms
    for every flat following the normal distribution. We change the non-positive values to 1
    and then we generate error values with normal distribution for our linear model.
    Finally, we calculate the amount of squared meters in the flat by linear model: 20+30 * rooms + error
    """
    df = pd.DataFrame(index=range(numberOfRows), columns=range(2))
    np.random.seed(100)
    rooms = pd.Series(np.round(np.random.normal(3.5,1.5,numberOfRows)), index=df.index)
    rooms[rooms<1]=1
    df[1] = rooms
    error = pd.Series(np.round(np.random.normal(10,5,numberOfRows)), index=df.index)
    meeters = 20+30 * rooms + error
    df[0] = meeters
    return df

def randomDataCreationFlats(numberOfRows):
    """
    We are creating random data describing set of flats. First we generate the number of rooms
    for every flat following the normal distribution. We change the non-positive values to 1
    and then we generate error values with normal distribution for our linear model.
    Finally, we calculate the amount of squared meters in the flat by linear model: 20+30 * rooms + error
    """
    df = pd.DataFrame(index=range(numberOfRows), columns=range(3))
    np.random.seed(100)
    rooms = pd.Series(np.round(np.random.normal(3.5,1.5,numberOfRows)), index=df.index)
    rooms[rooms<1]=1
    df[1] = rooms
    error = pd.Series(np.round(np.random.normal(10,5,numberOfRows)), index=df.index)
    meeters = 20+30 * rooms + error
    df[0] = meeters
    distance = pd.Series(np.abs(np.round(np.random.normal(0,2000,numberOfRows))), index=df.index)
    df[2] = distance
    return df

def topKFormat(data_frame):
    """
    Function topkFormat takes pandas.Dataframe object and creates as many Dataframes
    as there are columns in data_frame. All new data frames are sorted and returned as
    a list.
    The first data frame contains all the data the rest contains only distinct values of
    all columns except the first one.
    :type data_frame: pd.DataFrame
    """
    assert type(data_frame) is pd.DataFrame, "Wrong type of input parameter"
    listOfTables = []
    for column in (data_frame.columns):
        newDataFrame = pd.DataFrame(data_frame[column])
        listOfTables.append(newDataFrame.sort([column]))
    return listOfTables

def topKFormatRich(data_frame):
    """
    Function topkFormat takes pandas.Dataframe object and creates as many Dataframes
    as there are columns in data_frame. All new data frames are sorted and returned as
    a list.
    The first data frame contains all the data the rest contains only distinct values of
    all columns except the first one.
    :type data_frame: pd.DataFrame
    """
    assert type(data_frame) is pd.DataFrame, "Wrong type of input parameter"
    listOfTables = []
    for column in (data_frame.columns):
        newDataFrame = pd.DataFrame(data_frame.sort([column]))
        listOfTables.append(newDataFrame)
    return listOfTables